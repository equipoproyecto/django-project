from django.conf import settings
from django.db import models
from django.utils import timezone

class Correo(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    emisor = models.CharField(max_length=60)
    asunto = models.CharField(max_length=200)
    receptor = models.CharField(max_length=60)
    mensaje = models.TextField()
    fecha_envio = models.DateTimeField(default=timezone.now)

    def envio(self):
        self.fecha_envio = timezone.now()
        self.save()

    def __str__(self):
        return self.asunto