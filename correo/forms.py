from django import forms
from .models import Correo

class CorreoForm(forms.ModelForm):
    class Meta:
        model = Correo
        fields = ('asunto', 'mensaje', 'receptor')