from django.shortcuts import redirect, render
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone
from mysite.settings import EMAIL_HOST_USER
from .models import Correo
from .forms import CorreoForm

def enviarcorreo(request):
    if request.method=="POST":
        form = CorreoForm(request.POST)
        if form.is_valid():
           
            correo = form.save(commit=False)
            correo.author = request.user
            correo.emisor = "si"
            correo.fecha_envio = timezone.now()
            correo.save()

            send_mail(correo.asunto, correo.mensaje, settings.EMAIL_HOST_USER, [correo.receptor])

            return redirect('/correo_list/')
    else:
        form = CorreoForm()
    return render(request,"contacto.html", {'form': form })

def mostrarmenu(request):
    if request.method=="POST":
        return redirect('enviarcorreo/')

    return render(request,"menu.html")

def correo_list(request):
    enviados = Correo.objects.filter(fecha_envio__lte=timezone.now()).order_by('fecha_envio')
    return render(request, "correo_list.html", {'enviados': enviados})


    
# Create your views here.
